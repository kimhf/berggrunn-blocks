<?php
/**
 * Setup plugin resources
 *
 * @package berggrunn-blocks
 */

namespace Berggrunn\Blocks;

/**
 * Get a value from the plugin config.
 *
 * @param string $key The key to desired config value.
 *
 * @return string The config value.
 */
function config($key)
{
    static $config;
    if (! $config) {
        $config = setup_config();
    }
    if (array_key_exists($key, $config)) {
        return $config[ $key ];
    }
    return '';
}
