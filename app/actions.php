<?php
namespace Berggrunn\Blocks;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Gutenberg block
 */
add_action('init', function () {
    $version = config('plugin.version');

    wp_localize_script('jquery', 'BERGGRUNN_DIST_PATH', trailingslashit(config('assets.uri')));

    /*
    wp_register_script('berggrunn/scripts/bootstrap/util', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/util.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/alert', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/alert.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/button', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/button.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/carousel', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/carousel.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/collapse', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/collapse.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/dropdown', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/dropdown.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/modal', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/modal.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/scrollspy', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/scrollspy.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/tab', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/tab.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/tooltip', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/tooltip.js'), ['jquery'], $version, true);
    wp_register_script('berggrunn/scripts/bootstrap/popover', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap/popover.js'), ['jquery'], $version, true);
    */

    wp_register_script('berggrunn/scripts/bootstrap', \Berggrunn\Blocks\Assets::asset_path('scripts/bootstrap.js'), ['jquery'], $version, true);

    wp_register_style('berggrunn/styles/components/main', \Berggrunn\Blocks\Assets::asset_path('styles/components/main.css'), null, $version);

    wp_register_script('berggrunn/scripts/shortcodes/email', \Berggrunn\Blocks\Assets::asset_path('scripts/shortcodes/email.js'), ['jquery'], $version, true);
}, 10);
