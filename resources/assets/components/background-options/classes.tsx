/**
 * Set inline CSS class.
 * @param {object} props - The block object.
 * @return {array} The inline CSS class.
 */
function backgroundOptionsClasses(props) {
  const { backgroundType, backgroundImage, dimRatio, hasParallax, backgroundVideo } = props.attributes
  return [
    { 'has-background-image has-custom-background': 'image' === backgroundType && backgroundImage },
    { 'has-background-color has-custom-background': 'color' === backgroundType },
    { 'has-background-video has-custom-background': 'video' === backgroundType && backgroundVideo },
    { 'has-background-dim': dimRatio !== 0 && ['image', 'video'].includes(backgroundType) && (backgroundVideo || backgroundImage) },
    { [dimRatioToClass(dimRatio)]: ['image', 'video'].includes(backgroundType) && (backgroundVideo || backgroundImage) },
    { 'has-parallax': hasParallax && 'image' === backgroundType && backgroundImage }
  ];
}

function dimRatioToClass(ratio) {
  return (ratio === 0 || ratio === 50) ?
    '' :
    'has-background-dim-' + (10 * Math.round(ratio / 10));
}

export default backgroundOptionsClasses;
