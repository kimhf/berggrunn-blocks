/** @jsx wp.element.createElement */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

const {
  ColorPalette,
  MediaUpload,
} = wp.editor;

const {
  IconButton,
  PanelBody,
  SelectControl,
  ToggleControl,
  RangeControl,
} = wp.components;

/**
 * Internal dependencies
 */
import backgroundOptionsAttributes from './attributes';
import backgroundOptionsClasses from './classes';
import backgroundOptionsInlineStyles from './inline-styles';
import BackgroundOptionsVideoOutput from './video';
// import './editor.scss';

const ImageBackgroundSelect = ({ props }) => {
  const setBackgroundImage = value => props.setAttributes({ backgroundImage: {
    id: value.id,
    url: value.url
  }});
  const removeBackgroundImage = () => props.setAttributes({ backgroundImage: null });
  const toggleParallax = () => props.setAttributes({ hasParallax: !props.attributes.hasParallax });
  const setDimRatio = (ratio) => props.setAttributes({ dimRatio: ratio });

  if (!props.attributes.backgroundImage) {
    const renderMediaUploadButton = ({ open }) => (
      <IconButton
        style={{ display: 'flex', }}
        className="button button-large"
        onClick={open}
        icon="format-image"
      >
        {__('Upload Image')}
      </IconButton>
    )

    return (
      <div>
        <p>
          <MediaUpload
            onSelect={setBackgroundImage}
            type="image"
            value=""
            render={renderMediaUploadButton}
          />
        </p>
        <p>
          {__('Add/Upload an image file. (.jpg, .png)')}
        </p>
      </div>
    );
  }

  return (
    <div>
      <p>
        <img
          src={props.attributes.backgroundImage.url}
          alt={props.attributes.backgroundImage.alt}
        />
      </p>
      <p>
        <IconButton
          style={{ display: 'flex', }}
          className="remove-image button button-large"
          onClick={removeBackgroundImage}
          icon="no-alt"
        >
          {__('Remove Image')}
        </IconButton>
      </p>
      <p>
        {__('Add/Upload an image file. (.jpg, .png)')}
      </p>
      <ToggleControl
        label={__('Fixed Background')}
        checked={!!props.attributes.hasParallax}
        onChange={toggleParallax}
      />
      <RangeControl
        label={__('Background Dimness')}
        value={props.attributes.dimRatio}
        onChange={setDimRatio}
        min={0}
        max={100}
        step={10}
      />
    </div>
  );
};

const VideoBackgroundSelect = ({ props }) => {
  const setDimRatio = (ratio) => props.setAttributes({ dimRatio: ratio });
  const setBackgroundVideo = value => props.setAttributes({ backgroundVideo: value });
  const removeBackgroundVideo = () => props.setAttributes({ backgroundVideo: null });

  if (!props.attributes.backgroundVideo) {

    const renderMediaUploadButton = ({ open }) => (
      <IconButton
        style={{ display: 'flex', }}
        className="button button-large"
        onClick={open}
        icon="format-video"
      >
        {__('Upload Video')}
      </IconButton>
    )

    return (
      <div>
        <p>
          <MediaUpload
            onSelect={setBackgroundVideo}
            type="video"
            value=""
            render={renderMediaUploadButton}
          />
        </p>
        <p>
          {__('Add/Upload a .mp4 video file.')}
        </p>
      </div>
    );
  }

  return (
    <div>
      <p>
        <BackgroundOptionsVideoOutput props={props} autoPlay={false} />
      </p>
      <p>
        <IconButton
          style={{ display: 'flex', }}
          className="remove-video button button-large"
          onClick={removeBackgroundVideo}
          icon="no-alt"
        >
          {__('Remove Video')}
        </IconButton>
      </p>
      <p>
        {__('Add/Upload a 1920x1080 .mp4 video file.')}
      </p>
      <RangeControl
        label={__('Background Dimness')}
        value={props.attributes.dimRatio}
        onChange={setDimRatio}
        min={0}
        max={100}
        step={10}
      />
    </div>
  );
};

const ColorSelect = ({ props }) => {
  const setBackgroundColor = value => props.setAttributes({ backgroundColor: value });

  return (
    <ColorPalette
      value={props.attributes.backgroundColor}
      onChange={setBackgroundColor}
    />
  );
};

function BackgroundOptionsInspectorControls({ props }) {
  const setBackgroundType = value => props.setAttributes({ backgroundType: value });

  return (
    <PanelBody
      title={__('Background Options')}
      initialOpen={true}
    >
      <SelectControl
        style={{ height: 'inherit', }}
        key="background-type"
        label={__('Background Type')}
        value={props.attributes.backgroundType ? props.attributes.backgroundType : ''}
        options={[
          {
            label: __('None'),
            value: '',
          },
          {
            label: __('Image'),
            value: 'image',
          },
          {
            label: __('Video'),
            value: 'video',
          },
          {
            label: __('Color'),
            value: 'color',
          },
        ]}
        onChange={setBackgroundType}
      />
      {'video' === props.attributes.backgroundType &&
        <VideoBackgroundSelect props={props} />
      }
      {'image' === props.attributes.backgroundType &&
        <ImageBackgroundSelect props={props} />
      }
      {'color' === props.attributes.backgroundType &&
        <ColorSelect props={props} />
      }
    </PanelBody>
  );
}

// Export for ease of importing in individual blocks.
export {
  BackgroundOptionsInspectorControls,
  backgroundOptionsAttributes,
  backgroundOptionsClasses,
  backgroundOptionsInlineStyles,
  BackgroundOptionsVideoOutput,
};
