<?php
namespace Berggrunn\Components\BackgroundOptions;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Set inline CSS class.
 * @param {object} props - The block object.
 * @return {array} The inline CSS class.
 */
function backgroundOptionsClasses($attributes)
{
    $args = wp_parse_args($attributes, [
        'backgroundType' => '',
        'backgroundImage' => '',
        'dimRatio' => 0,
        'hasParallax' => '',
        'backgroundVideo' => '',
    ]);

    $classes = [];

    if ('image' === $args['backgroundType'] && $args['backgroundImage']) {
        $classes[] = 'has-background-image has-custom-background';
    }

    if ('color' === $args['backgroundType']) {
        $classes[] = 'has-background-color has-custom-background';
    }

    if ('video' === $args['backgroundType'] && $args['backgroundVideo']) {
        $classes[] = 'has-background-video has-custom-background';
    }

    if ($args['dimRatio'] !== 0 && in_array($args['backgroundType'], ['image', 'video']) && ($args['backgroundVideo'] || $args['backgroundImage'])) {
        $classes[] = 'has-background-dim';
    }

    if (in_array($args['backgroundType'], ['image', 'video']) && ($args['backgroundVideo'] || $args['backgroundImage'])) {
        $classes[] = dimRatioToClass($args['dimRatio']);
    }

    if ($args['hasParallax'] && 'image' === $args['backgroundType'] && $args['backgroundImage']) {
        $classes[] = 'has-parallax';
    }

    return implode(' ', $classes);
}

function dimRatioToClass($ratio)
{
    return ($ratio === 0 || $ratio === 50) ?
        '' :
        'has-background-dim-' . (10 * intval($ratio / 10));
}

/**
 * Set inline styles.
 *
 * @param  {object} props - The block object.
 * @param  {string} context - save or edit context.
 * @return {object} The inline background type CSS.
 */
function backgroundOptionsInlineStyles($attributes)
{
    $args = wp_parse_args($attributes, [
        'backgroundType' => '',
        'backgroundColor' => '',
        'backgroundImage' => null,
    ]);

    switch ($args['backgroundType']) {
        case 'color':
            if ($args['backgroundColor']) {
                return 'background-color: '.$args['backgroundColor'].';';
            }
            break;
        case 'image':
            if ($args['backgroundImage'] && $args['backgroundImage']['url']) {
                return 'background-image: url('.$args['backgroundImage']['url'].');';
            }
            break;
        default:
            return '';
        break;
    }
    return '';
}
