/**
 * Set inline styles.
 *
 * @param  {object} props - The block object.
 * @param  {string} context - save or edit context.
 * @return {object} The inline background type CSS.
 */
function backgroundOptionsInlineStyles(props) {
  switch (props.attributes.backgroundType) {
    case 'color':
      if (props.attributes.backgroundColor) {
        return { backgroundColor: props.attributes.backgroundColor }
      }
      break;
    case 'image':
      if (props.attributes.backgroundImage && props.attributes.backgroundImage.url) {
        return { backgroundImage: `url(${props.attributes.backgroundImage.url})` }
      }
      break;
  }
  return {}
}

export default backgroundOptionsInlineStyles;
