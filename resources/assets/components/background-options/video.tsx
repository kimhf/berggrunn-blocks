/** @jsx wp.element.createElement */

/**
 * Set video output.
 * @param {object} props - The block object.
 * @return {string} The video output container.
 */
function BackgroundOptionsVideoOutput({ props, autoPlay = false }) {
  if ('video' === props.attributes.backgroundType && props.attributes.backgroundVideo) {
    return (
      <video
        className="video-container video-container-overlay"
        autoPlay={autoPlay}
        loop={true}
        muted={true}
      >
        <source
          type="video/mp4"
          src={props.attributes.backgroundVideo.url}
        />
      </video>
    );
  }

  return null
}

export default BackgroundOptionsVideoOutput;
