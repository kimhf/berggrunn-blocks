/**
 * Set the attributes to be displayed in the Background Options panel.
 * @type {Object}
 */

const backgroundOptionsAttributes = {
  backgroundColor: {
    type: 'string',
  },
  backgroundImage: {
    type: 'object',
  },
  backgroundType: {
    default: '',
    type: 'string',
  },
  backgroundVideo: {
    type: 'object',
  },
  dimRatio: {
    default: 50,
    type: 'number',
  },
  hasParallax: {
    default: false,
    type: 'boolean',
  },

};

export default backgroundOptionsAttributes;
