'use strict'; // eslint-disable-line

const webpack = require('webpack')
const merge = require('webpack-merge')
const CleanPlugin = require('clean-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
// const StyleLintPlugin = require('stylelint-webpack-plugin');
const EasyStylelintPlugin = require('easy-stylelint-plugin')
// const CopyGlobsPlugin = require('copy-globs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

const config = require('./config')

const assetsFilenames = (config.enabled.cacheBusting) ? config.cacheBusting : '[name]'

const IconfontWebpackPlugin = require('iconfont-webpack-plugin')

const path = require('path')
const glob = require('glob-all')

let webpackConfig = {
  mode: config.env.production ? 'production' : 'development',
  context: config.paths.assets,
  entry: config.entry,
  devtool: (config.enabled.sourceMaps ? '#source-map' : undefined),
  output: {
    path: config.paths.dist,
    publicPath: config.publicPath,
    filename: `scripts/${assetsFilenames}.js`,
  },
  stats: {
    hash: false,
    version: false,
    timings: false,
    children: false,
    errors: true,
    errorDetails: true,
    warnings: false,
    chunks: false,
    modules: false,
    reasons: false,
    source: false,
    publicPath: false,
  },
  module: {
    rules: [
      {
        test: /\.modernizrrc.js$/,
        loader: 'modernizr',
      },
      {
        test: /\.modernizrrc(\.json)?$/,
        loader: 'modernizr!json',
      },
      {
        enforce: 'pre',
        test: /\.(js|jsx)$/,
        include: config.paths.assets,
        use: 'eslint',
      },
      /*
      {
        enforce: 'pre',
        test: /\.(ts|tsx)$/,
        include: config.paths.assets,
        use: 'tslint',
      },
      */
      {
        enforce: 'pre',
        test: /\.(js|jsx|s?[ca]ss)$/,
        include: config.paths.assets,
        loader: 'import-glob',
      },
      /*
      {
        test: /\.(js|jsx)$/,
        exclude: [/(node_modules|bower_components)(?![/|\\](bootstrap|foundation-sites))/],
        use: [
          { loader: 'cache' },
          { loader: 'buble', options: { objectAssign: 'Object.assign' } },
        ],
      },
      */
      {
        test: /\.(js|jsx|mjs)$/,
        include: config.paths.assets,
        // exclude: [/(node_modules|bower_components)(?![/|\\](bootstrap|foundation-sites))/],
        // loader: require.resolve('babel-loader'),
        use: [
          { loader: 'cache' },
          { loader: 'babel', options: { compact: true } },
        ],
        /*
        options: {
          compact: true,
        },
        */
      },
      {
        test: /\.(ts|tsx)$/,
        include: config.paths.assets,
        use: [
          { loader: 'cache' },
          {
            loader: 'ts',
            options: {
              // disable type checker - we will use it in fork plugin
              transpileOnly: true,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        include: config.paths.assets,
        use: ExtractTextPlugin.extract({
          fallback: 'style',
          use: [
            { loader: 'cache' },
            { loader: 'css', options: { sourceMap: config.enabled.sourceMaps } },
            {
              loader: 'postcss',
              options: {
                config: { path: __dirname, ctx: config },
                sourceMap: config.enabled.sourceMaps,
                plugins: (loader) => [
                  // Add the plugin
                  new IconfontWebpackPlugin(loader),
                ],
              },
            },
          ],
        }),
      },
      {
        test: /\.scss$/,
        include: config.paths.assets,
        use: ExtractTextPlugin.extract({
          fallback: 'style',
          use: [
            { loader: 'cache' },
            { loader: 'css', options: { sourceMap: config.enabled.sourceMaps } },
            {
              loader: 'postcss',
              options: {
                config: { path: __dirname, ctx: config },
                sourceMap: config.enabled.sourceMaps,
                plugins: (loader) => [
                  // Add the plugin
                  new IconfontWebpackPlugin(loader),
                ],
              },
            },
            { loader: 'resolve-url', options: { sourceMap: config.enabled.sourceMaps } },
            {
              loader: 'sass',
              options: {
                sourceMap: config.enabled.sourceMaps,
                includePaths: glob.sync(
                  path.join(__dirname, '../../../node_modules/@material')
                ).map((dir) => path.dirname(dir)),
              },
            },
          ],
        }),
      },
      {
        test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg|ico)$/,
        include: config.paths.assets,
        loader: 'url',
        options: {
          limit: 4096,
          name: `[path]${assetsFilenames}.[ext]`,
        },
      },
      {
        test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg|ico)$/,
        include: /node_modules|bower_components/,
        loader: 'url',
        options: {
          limit: 4096,
          outputPath: 'vendor/',
          name: `${config.cacheBusting}.[ext]`,
        },
      },
    ],
  },
  resolve: {
    extensions: [
      '.mjs',
      '.web.ts',
      '.ts',
      '.web.tsx',
      '.tsx',
      '.web.js',
      '.js',
      '.json',
      '.web.jsx',
      '.jsx',
    ],
    modules: [
      config.paths.assets,
      'node_modules',
      'bower_components',
    ],
    enforceExtension: false,
    // https://github.com/Modernizr/Modernizr/blob/master/lib/config-all.json
    alias: {
      modernizr$: path.resolve(__dirname, '.modernizrrc'),
    },
  },
  resolveLoader: {
    moduleExtensions: ['-loader'],
  },
  externals: {
    jquery: 'jQuery',
  },
  plugins: [
    new CleanPlugin([config.paths.dist], {
      root: config.paths.root,
      verbose: false,
    }),
    /**
     * It would be nice to switch to copy-webpack-plugin, but
     * unfortunately it doesn't provide a reliable way of
     * tracking the before/after file names
     */
    /* new CopyGlobsPlugin({
      pattern: config.copy,
      output: `[path]${assetsFilenames}.[ext]`,
      manifest: config.manifest,
    }), */
    new ExtractTextPlugin({
      filename: `styles/${assetsFilenames}.css`,
      allChunks: true,
      disable: (config.enabled.watcher),
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: 'popper.js/dist/umd/popper.js',
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: config.enabled.optimize,
      debug: config.enabled.watcher,
      stats: { colors: true },
    }),
    new webpack.LoaderOptionsPlugin({
      test: /\.s?css$/,
      options: {
        output: { path: config.paths.dist },
        context: config.paths.assets,
      },
    }),
    new webpack.LoaderOptionsPlugin({
      test: /\.js$/,
      options: {
        eslint: { failOnWarning: false, failOnError: true },
      },
    }),
    /* new StyleLintPlugin({
      failOnError: false,//!config.enabled.watcher,
      emitErrors: true,
      quiet: false,
      syntax: 'scss',
    }), */
    new EasyStylelintPlugin({
      failOnError: !config.enabled.watcher,
      syntax: 'scss',
    }),
    new FriendlyErrorsWebpackPlugin(),
    // Perform type checking and linting in a separate process to speed up compilation
    new ForkTsCheckerWebpackPlugin({
      async: false,
      watch: config.enabled.watcher ? config.paths.assets : null,
      tsconfig: config.paths.appTsConfig,
      tslint: config.paths.appTsLint,
    }),
  ],
  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  },
  // Turn off performance hints during development because we don't do any
  // splitting or minification in interest of speed. These warnings become
  // cumbersome.
  performance: {
    hints: false,
  },
}

/* eslint-disable global-require */ /** Let's only load dependencies as needed */

if (config.enabled.optimize) {
  webpackConfig = merge(webpackConfig, require('./webpack.config.optimize'))
}

if (config.env.production) {
  webpackConfig.plugins.push(new webpack.NoEmitOnErrorsPlugin())
}

const copyPluginOptions = []
const loaderUtils = require('loader-utils')

//
// Copy files from node_modules based on config.copyNodeModules
//
if (typeof config.copyNodeModules !== 'undefined' && config.copyNodeModules.length) {
  // Transform the copyNodeModules config from array to object
  const copyNodeModules = {}
  config.copyNodeModules.forEach((module) => {
    copyNodeModules[module.from] = module
  })

  const copyBaseConfig = {
    context: path.join(config.paths.root, 'node_modules'),
    toType: 'file',
  }

  copyPluginOptions.push(...Object.keys(copyNodeModules).map(file => {
    const copyPluginOption = Object.assign({}, copyNodeModules[file], copyBaseConfig)

    if (config.enabled.cacheBusting) {
      if (copyPluginOption.toType === 'file') {
        copyPluginOption.to = loaderUtils.interpolateName({ resourcePath: copyNodeModules[file].to }, `[path]${assetsFilenames}.[ext]`, { content: copyNodeModules[file].from })
      } /* else if (copyPluginOption.toType === 'template') {
        // This probably needs some work to replace the path?
        copyPluginOption.to = `[path]${assetsFilenames}.[ext]`
      }
      */
    }

    return copyPluginOption
  }))
}

//
// Copy files from assets based on config.copy
//
if (config.enabled.cacheBusting) {
  const files = glob.sync(config.copy, {
    nodir: true,
    cwd: config.paths.assets,
  })

  copyPluginOptions.push(...files.map(file => {
    return {
      from: file,
      to: loaderUtils.interpolateName({ resourcePath: file }, `[path]${assetsFilenames}.[ext]`, { content: file }),
      toType: 'file',
    }
  }))
} else {
  copyPluginOptions.push({
    from: config.copy,
    to: `[path]${assetsFilenames}.[ext]`,
    toType: 'template',
  })
}

if (config.enabled.cacheBusting) {
  const WebpackAssetsManifest = require('webpack-assets-manifest')

  const assetsManifest = new WebpackAssetsManifest({
    output: 'assets.json',
    space: 2,
    writeToDisk: false,
    assets: config.manifest,
    replacer: require('./util/assetManifestsFormatter'),
  })

  webpackConfig.plugins.push(assetsManifest)

  assetsManifest.hooks.apply.tap('SageApply', manifest => {
    for (let index = 0; index < copyPluginOptions.length; index++) {
      const option = copyPluginOptions[index]
      manifest.set(option.from, option.to)
    }
  })
}

webpackConfig.plugins.push(new CopyWebpackPlugin(copyPluginOptions))

if (config.enabled.watcher) {
  webpackConfig.entry = require('./util/addHotMiddleware')(webpackConfig.entry)
  webpackConfig = merge(webpackConfig, require('./webpack.config.watch'))
}

module.exports = webpackConfig
