/* eslint-env browser */
/* globals __resourceQuery, FIXING_STYLE_ENTERY_POINT */
if (typeof FIXING_STYLE_ENTERY_POINT === 'undefined' || !FIXING_STYLE_ENTERY_POINT) {
  FIXING_STYLE_ENTERY_POINT = true // eslint-disable-line no-global-assign
  if (__resourceQuery) {
    var querystring = require('querystring')
    var parsedQuerystring = querystring.parse(__resourceQuery.slice(1))
    var settings = JSON.parse(parsedQuerystring.settings)

    if (settings && settings.styleEntrypoints && settings.styleEntrypoints.length && document.styleSheets && document.styleSheets.length) {
      const styleSheets = document.styleSheets
      const styleEntrypoints = settings.styleEntrypoints
      for (var i = 0; i < styleSheets.length; i++) {
        const styleSheet = styleSheets[i]
        styleEntrypoints.forEach(id => {
          if (styleSheet.href && styleSheet.href.match(`styles/${id}.css`)) {
            const script = document.createElement('script')
            script.type = 'text/javascript'
            script.src = styleSheet.href.replace('styles', 'scripts').replace('.css', '.js')
            document.getElementsByTagName('head')[0].appendChild(script)

            // console.log(`Injected script ${script.src}`)
          }
        })
      }
    }
  }
}
