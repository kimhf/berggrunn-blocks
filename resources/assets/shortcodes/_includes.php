<?php

namespace Berggrunn\Shortcodes;

/**
 * Required shortcode files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($shortcode) {
    $file = __DIR__ . "/{$shortcode}/index.php";
    if (!file_exists($file)) {
        sage_error(sprintf(__('Error locating shortcode <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    } else {
        require_once($file);
        add_shortcode($shortcode, __NAMESPACE__ . "\\render_shortcode__{$shortcode}");
    }
}, [
    'email',
    'year',
    'blogname'
]);
