<?php

namespace Berggrunn\Shortcodes;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function render_shortcode__email($atts, $content = "")
{
    wp_enqueue_script('berggrunn/scripts/shortcodes/email');

    $result = '';
    if (isset($atts['address']) && $atts['address']) {
        $address = $atts['address'];
        $result .= get_email_super_antispambot($address);
    }

    if ($content) {
        $result .= get_fixed_emails($content);
    }
    return $result;
}

/**
 * Automatically hide email adresses from spambots on your WordPress blog
 */
function get_fixed_emails($content)
{
    $pattern = '/(<a .*)href=(")mailto:([^"]*)"([^>]*)>([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})<\/a>/U';

    $content = preg_replace_callback($pattern, "security_remove_emails_atags", $content);

    $pattern = '/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/i';//

    $content = preg_replace_callback($pattern, "security_remove_emails_logic", $content);

    return $content;
}

function security_remove_emails_atags($subject)
{
    $pattern = '/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/i';
    preg_match($pattern, $subject[3], $matches, PREG_OFFSET_CAPTURE, 3);
    if (!empty($matches)) {
        return $subject[3];
    }
    return '';
}

function security_remove_emails_logic($result)
{
    return get_email_super_antispambot($result[0]);
}

/**
 *  Try to hide email from web crawlers, http://www.maurits.vdschee.nl/php_hide_email/ + antispambot();, Javascript in seperate file, Css to display corectly before mouse over event
 */
function get_email_super_antispambot($email)
{
    if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return $email;
    }
    $emailparts = explode('@', $email);
    $emailFront = $emailparts[0];
    $emailFront = strrev(str_rot13($emailFront));
    $emailBack = $emailparts[1];
    $emailBack = strrev(str_rot13($emailBack));

    //$character_set = "+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
    $character_set = "+-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
    $key = str_shuffle($character_set);
    $emailBackCipher = '';
    for ($i=0; $i<strlen($emailBack);
    $i+=1) {
        $emailBackCipher.= $key[strpos($character_set, $emailBack[$i])];
    }

    return '<span class="cursorium" data-a="'.$key.'" data-c="'.$emailBackCipher.'" data-q="'.antispambot($emailFront).'"></span>';
}
