/* global jQuery */

jQuery(($) => {
  function reverse(s) {
    return s.split("").reverse().join("");
  }
  function str_rot13(str) {
    return (str + '').replace(/[a-z]/gi, (s) => {
      return String.fromCharCode(s.charCodeAt(0) + (s.toLowerCase() < 'n' ? 13 : -13));
    });
  }

  $('.cursorium').each(function () {
    const a = $(this).data('a');
    const b = a.split("").sort().join("");
    const c = $(this).data('c');
    let d = "";
    const q = $(this).data('q');

    for (let e = 0; e < c.length; e++) {
      d += b.charAt(a.indexOf(c.charAt(e)));
    }
    d = str_rot13(d);

    const eil = reverse(str_rot13(q)) + '@' + reverse(d);

    $(this).html('<a href="mailto:' + eil + '">' + eil + '</a>');
  });
});
