<?php

namespace Berggrunn\Shortcodes;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function render_shortcode__year($atts, $content = "")
{
    $year = date('Y');
    return $year;
}
