<?php

namespace Berggrunn\Shortcodes;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function render_shortcode__blogname($atts, $content = "")
{
    $blogname = get_bloginfo();
    return $blogname;
}
