export function autoExpandElement(el) {
  if (document.documentElement) {
    const scrollTop = document.documentElement.scrollTop

    el.style.overflow = 'hidden'
    el.style.height = 'inherit'
    el.style.height = (el.scrollHeight + 2) + 'px'

    document.documentElement.scrollTop = scrollTop

    el.style.overflow = 'auto'
  }
}

/**
 * @see https://gist.github.com/gordonbrander/2230317
 */
/*
const createId = () => {
  return Math.random().toString(36).substr(2, 9)
}
*/
