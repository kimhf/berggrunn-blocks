<?php
namespace Berggrunn\Blocks\Profile;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Gutenberg block
 */
add_action('init', function () {
    $version = \Berggrunn\Blocks\Plugin::get_plugin_version();

    wp_register_script('berggrunn/scripts/blocks/profile/editor', \Berggrunn\Blocks\Assets::asset_path('scripts/blocks/profile/editor.js'), ['wp-blocks', 'wp-element'], $version, false);
    wp_register_style('berggrunn/styles/blocks/profile/editor', \Berggrunn\Blocks\Assets::asset_path('styles/blocks/profile/editor.css'), ['wp-edit-blocks'], $version);

    wp_register_script('berggrunn/scripts/blocks/profile/main', \Berggrunn\Blocks\Assets::asset_path('scripts/blocks/profile/main.js'), ['jquery', 'berggrunn/scripts/bootstrap/collapse'], $version, true);
    wp_register_style('berggrunn/styles/blocks/profile/main', \Berggrunn\Blocks\Assets::asset_path('styles/blocks/profile/main.css'), null, $version);

    if (!function_exists('register_block_type')) {
        return;
    }

    register_block_type('berggrunn/profile', array(
        'editor_script' => 'berggrunn/scripts/blocks/profile/editor',
        'editor_style'  => 'berggrunn/styles/blocks/profile/editor',
        'render_callback' => 'Berggrunn\\Blocks\\Profile\\render',
        // 'script' => 'berggrunn/scripts/blocks/profile/main',
        'style' => 'berggrunn/styles/blocks/profile/main',
        'attributes' => [
            'anchor' => [
                'type' => 'string'
            ],
            'title' => [
                'type' => 'string'
            ],
            'titleTag' => [
                'type' => 'string',
                'default' => 'h3'
            ],
            'subtitle' => [
                'type' => 'string'
            ],
            'subtitleTag' => [
                'type' => 'string',
                'default' => 'h4'
            ],
            'content' => [
                'type' => 'string'
            ],
            'className' => [
                'type' => 'string'
            ],
            'componentClassName' => [
                'type' => 'string'
            ],
            'contentAlign' => [
                'type' => 'string',
                'default' => 'left'
            ],
            'mediaURL' => [
                'type' => 'string'
            ],
            'mediaId' => [
                'type' => 'any'
            ],
            'align' => [
                'type' => 'string',
                'default' => 'center'
            ]
        ]
    ));
}, 20);

function render($attributes)
{
    // wp_enqueue_style('berggrunn/styles/blocks/profile/main');

    $attributes = wp_parse_args($attributes, [
        'anchor' => '',
        'title' => '',
        'titleTag' => 'h3',
        'subtitle' => '',
        'subtitleTag' => 'h4',
        'content' => '',
        'className' => '',
        'componentClassName' => '',
        'contentAlign' => 'left',
        'mediaURL' => '',
        'mediaId' => null,
        'align' => 'center'
    ]);

    $anchor = trim(esc_attr($attributes['anchor']));
    $title = trim(esc_attr($attributes['title']));
    $titleTag = trim(esc_attr($attributes['titleTag']));
    $subtitle = trim(esc_attr($attributes['subtitle']));
    $subtitleTag = trim(esc_attr($attributes['subtitleTag']));
    $content = wpautop(trim(esc_textarea($attributes['content'])));
    $className = trim(esc_attr($attributes['className']));
    $componentClassName = trim(esc_attr($attributes['componentClassName']));
    $subtitle = trim(esc_attr($attributes['subtitle']));
    $contentAlign = trim(esc_attr($attributes['contentAlign']));
    $mediaURL = trim(esc_url($attributes['mediaURL']));
    $mediaId = trim(esc_attr($attributes['mediaId']));
    $align = trim(esc_attr($attributes['align']));
    $id = uniqid('-');

    $image = '';
    if ($mediaId) {
        $image = wp_get_attachment_image($mediaId, 'large', false, [
            'class' => 'wp-block-berggrunn-profile__img'
        ]);
    }

    $classNames = [];
    $classNames[] = 'wp-block-berggrunn-profile';
    if ($className) {
        $classNames[] = $className;
    }
    if ($image) {
        $classNames[] = 'has-image';
    }
    if ($align) {
        $classNames[] = "align$align";
    }
    $className = implode(' ', $classNames);

    $componentClassNames = [];
    $componentClassNames[] = 'wp-block-berggrunn-profile__component';
    if ($componentClassName) {
        $componentClassNames[] = $componentClassName;
    }
    $componentClassName = implode(' ', $componentClassNames);

    $textStyles = "text-align: {$contentAlign}";

    if ($title) {
        $title = "<{$titleTag} class='wp-block-berggrunn-profile__title'>{$title}</{$titleTag}>";
    }

    if ($subtitle) {
        $subtitle = "<{$subtitleTag} class='wp-block-berggrunn-profile__subtitle'>{$subtitle}</{$subtitleTag}>";
    }

    return "<div id='${anchor}' class='{$className}'>
        <div class='{$componentClassName}'>
            <div class='wp-block-berggrunn-profile__media'>
                {$image}
            </div>
            <div class='wp-block-berggrunn-profile__body' style='{$textStyles}'>
                {$title}
                {$subtitle}
                <div class='wp-block-berggrunn-profile__content'>
                {$content}
                </div>
            </div>
        </div>
    </div>";
}
