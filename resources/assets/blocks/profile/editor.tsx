/** @jsx wp.element.createElement */

/**
 * External dependencies
 */
import * as classnames from 'classnames'
// tslint:disable-next-line:no-implicit-dependencies
import * as React from 'react'

/**
 * Internal dependencies
 */
import { autoExpandElement } from './../../lib/helpers'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

const { Fragment } = wp.element

const {
  registerBlockType,
} = wp.blocks

const {
  Button,
  Dashicon,
  IconButton,
  PanelBody,
  TextControl,
  Toolbar
} = wp.components

const {
  BlockControls,
  InspectorControls,
  InspectorAdvancedControls,
  BlockAlignmentToolbar,
  MediaUpload,
  AlignmentToolbar,
} = wp.editor

/**
 * Regular expression matching invalid anchor characters for replacement.
 *
 * @type {RegExp}
 */
const ANCHOR_REGEX = /[\s#]/g;

const validAlignments = ['center', 'wide', 'full'];

const blockAttributes = {
  align: {
    default: 'center',
    type: 'string',
  },
  anchor: {
    default: '',
    type: 'string',
  },
  className: {
    default: '',
    type: 'string',
  },
  componentClassName: {
    default: '',
    type: 'string',
  },
  content: {
    default: '',
    type: 'string',
  },
  contentAlign: {
    default: 'left',
    type: 'string',
  },
  mediaId: {
    type: 'number',
  },
  mediaURL: {
    default: '',
    type: 'string',
  },
  subtitle: {
    default: '',
    type: 'string',
  },
  title: {
    default: '',
    type: 'string',
  },
}

const settings = {
  title: 'Profile',

  icon: 'id',

  category: 'widgets',

  attributes: blockAttributes,

  supports: {
    anchor: false,
    className: true,
    customClassName: false,
  },

  getEditWrapperProps(attributes) {
    const { align } = attributes;
    if (-1 !== validAlignments.indexOf(align)) {
      return { 'data-align': align };
    }
    return {}
  },

  edit(props) {
    const { attributes, className, setAttributes } = props
    const { title, subtitle, content } = attributes

    // const removeImage = () => props.setAttributes({ backgroundImage: null });
    // const setImage = value => props.setAttributes({ backgroundImage: value });

    const onSelectImage = (media) => {
      let mediaURL = media.url
      if (media.sizes && media.sizes.large && media.sizes.large.url) {
        mediaURL = media.sizes.large.url
      }
      setAttributes({
        mediaId: media.id,
        mediaURL,
      });
    };

    const onRemoveImage = () => {
      setAttributes({
        mediaId: 0,
        mediaURL: '',
      });
    }

    const imageInspectorControls = () => {
      const renderMediaUploadButton = ({ open }) => (
        <IconButton
          style={{ display: 'flex' }}
          className="button button-large"
          onClick={open}
          icon="format-image"
        >
          {__('Upload Image')}
        </IconButton>
      )

      const renderMediaUploadImage = ({ open }) => (
        <div
          onClick={open}
          style={{ minHeight: '20px' }}
        >
          <img
            src={attributes.mediaURL}
            style={{ display: 'block' }}
          />
        </div>
      )

      if (!attributes.mediaURL) {
        return (
          <div className="media-upload-wrapper">
            <MediaUpload
              onSelect={onSelectImage}
              type="image"
              value=''
              render={renderMediaUploadButton}
            />
            <p>
              {__('Add/Upload an image file. (.jpg, .png)')}
            </p>
          </div>
        );
      }

      return (
        <div className="image-wrapper">
          <MediaUpload
            onSelect={onSelectImage}
            type='image'
            value={attributes.mediaId}
            render={renderMediaUploadImage}
          />
          <p>
            {__('Click the image to edit or update')}
          </p>
          <p>
            <IconButton
              style={{ display: 'flex' }}
              className="remove-image button button-large"
              onClick={onRemoveImage}
              icon="no-alt"
            >
              {__('Remove Image')}
            </IconButton>
          </p>
        </div>
      );
    };

    const onChangeForm = (e) => {
      e.preventDefault()

      autoExpandElement(e.target)

      setAttributes({
        [e.target.name]: e.target.value
      })
    }

    const onChangeClassName = (nextValue) => {
      setAttributes({ className: nextValue });
    }

    const onChangeComponentClassName = (nextValue) => {
      setAttributes({ componentClassName: nextValue });
    }

    const onChangeAnchor = (nextValue) => {
      nextValue = nextValue.replace(ANCHOR_REGEX, '-');
      setAttributes({ anchor: nextValue });
    }

    const onChangeContentAlign = (nextValue) => {
      setAttributes({ contentAlign: nextValue });
    }

    const onChangeBlockAlignment = (nextValue) => {
      setAttributes({ align: nextValue });
    }

    const textAlignmentClass = [{ 'text-left': attributes.contentAlign === 'left', 'text-center': attributes.contentAlign === 'center', 'text-right': attributes.contentAlign === 'right' }];

    const blockNameStyle: React.CSSProperties = {
      fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
      padding: '3px 13px',
    }

    /*
    const mediaUploadWrapStyles = {
      backgroundImage: 'none'
    }

    if (mediaURL) {
      mediaUploadWrapStyles.backgroundImage = `url(${mediaURL})`
    }
    */

    const mediaButtonRender = ({ open }) => (
      <div>
        <Button
          onClick={open}
          className={classnames('components-button button button-large', { 'hidden': attributes.mediaURL })}
        >
          {__('Open Media Library')}
        </Button>
      </div>
    )

    const mediaBlockControlsButtonRender = ({ open }) => (
      <Button
        onClick={open}
        className='components-button components-icon-button components-toolbar__control'
      // ariaLabel={__('Edit image')}
      >
        <Dashicon icon='format-image' />
      </Button>
    )

    const controls = (
      <Fragment>
        <InspectorControls>
          <PanelBody title={__('Profile Picture')}>
            {imageInspectorControls()}
          </PanelBody>
          <PanelBody
            title={__('Alignment')}
            className={'blocktype-profile-block-alignment-inspector-controls'}
          >
            <BlockAlignmentToolbar
              value={attributes.align}
              onChange={onChangeBlockAlignment}
              controls={validAlignments}
            />
            <AlignmentToolbar
              value={attributes.contentAlign}
              onChange={onChangeContentAlign}
            />
          </PanelBody>
        </InspectorControls>
        <InspectorAdvancedControls>
          <TextControl
            label={__('Additional CSS Class', 'gutenberg')}
            value={attributes.className}
            onChange={onChangeClassName}
          />
          <TextControl
            label={__('Additional Inner Component CSS Class')}
            value={attributes.componentClassName}
            onChange={onChangeComponentClassName}
          />
          <TextControl
            label={__('HTML Anchor', 'gutenberg')}
            help={__('Anchors lets you link directly to a section on a page.', 'gutenberg')}
            value={attributes.anchor}
            onChange={onChangeAnchor}
          />
        </InspectorAdvancedControls>
        <BlockControls>
          <div style={blockNameStyle}>
            <small>{settings.title}</small>
          </div>
          <BlockAlignmentToolbar
            value={attributes.align}
            onChange={onChangeBlockAlignment}
            controls={validAlignments}
          />
          <AlignmentToolbar
            value={attributes.contentAlign}
            onChange={onChangeContentAlign}
          />
          <Toolbar>
            <MediaUpload
              onSelect={onSelectImage}
              type='image'
              value={attributes.mediaId}
              render={mediaBlockControlsButtonRender}
            />
          </Toolbar>
        </BlockControls>
      </Fragment>
    )

    return (
      <Fragment>
        {controls}
        <div className={classnames(className)}>
          <div className='wp-block-berggrunn-profile__component'>
            <div className='wp-block-berggrunn-profile__media'>
              <div className={classnames('wp-block-berggrunn-profile__mediaUploadWrap', { 'has-no-image': !attributes.mediaURL }, { 'has-image': attributes.mediaURL })}>
                {!attributes.mediaURL &&
                  <MediaUpload
                    onSelect={onSelectImage}
                    type='image'
                    value={attributes.mediaId}
                    render={mediaButtonRender}
                  />
                }
                {attributes.mediaURL &&
                  <div
                    className='wp-block-berggrunn-profile__imgWrap'
                  >
                    <img
                      className='wp-block-berggrunn-profile__img'
                      src={attributes.mediaURL}
                    />
                  </div>
                }
              </div>
            </div>
            <div className='wp-block-berggrunn-profile__body'>
              <input
                className={classnames('wp-block-berggrunn-profile__title', textAlignmentClass)}
                name='title'
                value={title}
                onChange={onChangeForm}
                // onFocus={setFocus}
                type='text'
                placeholder='Title'
              />
              <input
                className={classnames('wp-block-berggrunn-profile__subtitle', textAlignmentClass)}
                name='subtitle'
                value={subtitle}
                onChange={onChangeForm}
                // onFocus={setFocus}
                type='text'
                placeholder='Subtitle'
              />
              <textarea
                className={classnames('wp-block-berggrunn-profile__content', textAlignmentClass)}
                name='content'
                value={content}
                onChange={onChangeForm}
                // onFocus={setFocus}
                placeholder={__('Write a brief bio...')}
                ref={(textarea) => {
                  if (textarea) {
                    window.setTimeout(() => {
                      autoExpandElement(textarea)
                    }, 500);
                  }
                }}
              />
            </div>
          </div>
        </div>
      </Fragment>
    )
  },

  save() {
    // Rendering in PHP
    return null
  },
}

registerBlockType('berggrunn/profile', settings)
