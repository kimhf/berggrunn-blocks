/** @jsx wp.element.createElement */

/**
 * External dependencies
 */
// import * as React from 'react'
import * as classnames from 'classnames'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n

const { Fragment } = wp.element

const {
  registerBlockType,
} = wp.blocks

const {
  PanelBody,
  TextControl,
} = wp.components

const {
  BlockControls,
  InspectorControls,
  InspectorAdvancedControls,
  AlignmentToolbar,
} = wp.editor

/**
 * Internal dependencies
 */
import { autoExpandElement } from './../../lib/helpers'

/**
 * Regular expression matching invalid anchor characters for replacement.
 *
 * @type {RegExp}
 */
const ANCHOR_REGEX: RegExp = /[\s#]/g;

const blockAttributes = {
  anchor: {
    default: '',
    type: 'string',
  },
  className: {
    default: '',
    type: 'string',
  },
  contentAlign: {
    default: 'left',
    type: 'string',
  },
  contentBody: {
    type: 'string',
  },
  contentTitle: {
    type: 'string',
  },
}

const settings = {
  title: 'Collapsible',

  icon: 'text',

  category: 'layout',

  attributes: blockAttributes,

  supports: {
    anchor: false,
    className: true,
    customClassName: false,
  },

  edit(props) {
    const { attributes, className, setAttributes, isSelected } = props

    const onChangeForm = (e) => {
      e.preventDefault()

      autoExpandElement(e.target)

      setAttributes({
        [e.target.name]: e.target.value
      })
    }

    const onChangeContentAlignment = (contentAlign) => {
      setAttributes({ contentAlign });
    }

    const onChangeClassName = (nextValue) => {
      setAttributes({ className: nextValue });
    }

    const onChangeAnchor = (nextValue) => {
      nextValue = nextValue.replace(ANCHOR_REGEX, '-');
      setAttributes({ anchor: nextValue });
    }

    const textAlignmentClass = [{ 'text-left': attributes.contentAlign === 'left', 'text-center': attributes.contentAlign === 'center', 'text-right': attributes.contentAlign === 'right' }];

    /*
    const blockNameStyle: React.CSSProperties = {
      padding: '3px 13px',
      fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
    }
    */

    return (
      <Fragment>
        {isSelected &&
          <InspectorControls>
            <PanelBody
              title={__('Alignment')}
              className={'blocktype-collapsible-alignment-inspector-controls'}
            >
              <AlignmentToolbar
                // key='alignmentToolbar'
                value={attributes.contentAlign}
                onChange={onChangeContentAlignment}
              />
            </PanelBody>
          </InspectorControls>
        }
        {isSelected &&
          <InspectorAdvancedControls>
            <TextControl
              label={__('Additional CSS Class', 'gutenberg')}
              value={attributes.className}
              onChange={onChangeClassName}
            />
            <TextControl
              label={__('HTML Anchor', 'gutenberg')}
              help={__('Anchors lets you link directly to a section on a page.', 'gutenberg')}
              value={attributes.anchor}
              onChange={onChangeAnchor}
            />
          </InspectorAdvancedControls>
        }
        {isSelected &&
          <BlockControls>
            <div className='blockNameStyle'>
              <small>{settings.title}</small>
            </div>
            <AlignmentToolbar
              // key='alignmentToolbar'
              value={attributes.contentAlign}
              onChange={onChangeContentAlignment}
            />
          </BlockControls>
        }
        <div className={className}>
          <input
            className={classnames('contentTitle', textAlignmentClass)}
            name='contentTitle'
            value={attributes.contentTitle}
            onChange={onChangeForm}
            type='text'
            placeholder='placeholder'
          />
          <textarea
            className={classnames('contentBody', textAlignmentClass)}
            name='contentBody'
            value={attributes.contentBody}
            onChange={onChangeForm}
            placeholder='placeholder'
            ref={(textarea) => {
              if (textarea) {
                window.setTimeout(() => {
                  autoExpandElement(textarea)
                }, 500);
              }
            }}
          />
        </div>
      </Fragment>
    )
  },

  save() {
    // Rendering in PHP
    return null
  },
}

registerBlockType('berggrunn/collapsible', settings)
