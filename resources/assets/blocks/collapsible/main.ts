/* global jQuery */
import * as jQuery from 'jquery'

jQuery(($) => {
  $('.wp-block-berggrunn-collapsible').on('hidden.bs.collapse', function () {
    $(this).closest('.wp-block-berggrunn-collapsible').removeClass('is-shown').addClass('is-hidden')
  }).on('shown.bs.collapse', function () {
    $(this).closest('.wp-block-berggrunn-collapsible').removeClass('is-hidden').addClass('is-shown')
  })
})
