<?php
namespace Berggrunn\Blocks\Collapsible;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Gutenberg block
 */
add_action('init', function () {
    $version = \Berggrunn\Blocks\Plugin::get_plugin_version();

    wp_register_script('berggrunn/scripts/blocks/collapsible/editor', \Berggrunn\Blocks\Assets::asset_path('scripts/blocks/collapsible/editor.js'), ['wp-blocks', 'wp-element'], $version, false);

    wp_register_style('berggrunn/styles/blocks/collapsible/editor', \Berggrunn\Blocks\Assets::asset_path('styles/blocks/collapsible/editor.css'), ['wp-edit-blocks'], $version);

    wp_register_script('berggrunn/scripts/blocks/collapsible/main', \Berggrunn\Blocks\Assets::asset_path('scripts/blocks/collapsible/main.js'), ['jquery', 'berggrunn/scripts/bootstrap'], $version, true);

    wp_register_style('berggrunn/styles/blocks/collapsible/main', \Berggrunn\Blocks\Assets::asset_path('styles/blocks/collapsible/main.css'), [], $version);

    if (!function_exists('register_block_type')) {
        return;
    }

    register_block_type('berggrunn/collapsible', array(
        'editor_script'   => 'berggrunn/scripts/blocks/collapsible/editor',
        'editor_style'    => 'berggrunn/styles/blocks/collapsible/editor',
        'style'           => 'berggrunn/styles/blocks/collapsible/main',
        'render_callback' => 'Berggrunn\\Blocks\\Collapsible\\render',
        // 'script' => 'berggrunn/scripts/blocks/collapsible/main',
        'attributes' => [
            'anchor' => [
                'type' => 'string'
            ],
            'contentTitle' => [
                'type' => 'string'
            ],
            'contentBody' => [
                'type' => 'string'
            ],
            'className' => [
                'type' => 'string'
            ],
            'contentAlign' => [
                'type' => 'string',
                'default' => 'left'
            ]
        ]
    ));
}, 20);

function render($attributes, $content = '')
{
    wp_enqueue_script('berggrunn/scripts/blocks/collapsible/main');

    $attributes = wp_parse_args($attributes, [
        'anchor' => '',
        'contentTitle' => '',
        'contentBody' => '',
        'className' => '',
        'contentAlign' => 'left'
    ]);

    $anchor = trim(esc_attr($attributes['anchor']));
    $className = trim(esc_attr($attributes['className']));
    $contentTitle = trim(esc_attr($attributes['contentTitle']));
    $contentBody = wpautop(trim(esc_textarea($attributes['contentBody'])));
    $id = uniqid('-');

    $classNames = [];
    $classNames[] = 'wp-block-berggrunn-collapsible is-hidden';
    if ($className) {
        $classNames[] = $className;
    }
    $className = implode(' ', $classNames);

    $textStyles = "text-align: {$attributes['contentAlign']}";

    return "<div id='{$anchor}' class='{$className}'>
    <div class='wp-block-berggrunn-collapsible__header' id='heading{$id}'>

            <a href='#collapse{$id}' class='collapsed wp-block-berggrunn-collapsible__toggler' data-toggle='collapse' data-target='#collapse{$id}' aria-expanded='false' aria-controls='collapse{$id}'>
            <h5 class='wp-block-berggrunn-collapsible__heading' style='{$textStyles}'>
            {$contentTitle}
            </h5>
            </a>

    </div>
    <div id='collapse{$id}' class='wp-block-berggrunn-collapsible__collapse collapse' aria-labelledby='heading{$id}'>
      <div class='wp-block-berggrunn-collapsible__body' style='{$textStyles}'>{$contentBody}</div>
    </div>
  </div>";
}
