/** @jsx wp.element.createElement */

/**
 * External dependencies
 */
import * as classnames from 'classnames';

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n

const { Fragment } = wp.element

const {
  registerBlockType,
} = wp.blocks

const {
  PanelBody,
  TextControl,
} = wp.components

const {
  BlockControls,
  InspectorControls,
  InspectorAdvancedControls,
  BlockAlignmentToolbar,
  InnerBlocks,
} = wp.editor

/**
 * Internal dependencies
 */
import {
  backgroundOptionsAttributes,
  backgroundOptionsClasses,
  backgroundOptionsInlineStyles,
  BackgroundOptionsInspectorControls,
  BackgroundOptionsVideoOutput,
} from './../../components/background-options'

/**
 * Regular expression matching invalid anchor characters for replacement.
 *
 * @type {RegExp}
 */
const ANCHOR_REGEX: RegExp = /[\s#]/g;

const blockNameStyle: React.CSSProperties = {
  fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
  padding: '3px 13px',
}

const name = 'berggrunn/container';

const validAlignments = ['center', 'wide', 'full'];

const blockAttributes = { ...backgroundOptionsAttributes, ...{
  align: {
    default: 'full',
    type: 'string',
  },
  anchor: {
    default: '',
    type: 'string',
  },
  className: {
    default: 'entry-content',
    type: 'string',
  },
}};

const settings = {
  title: 'Container',

  icon: 'editor-contract',

  category: 'layout',

  attributes: blockAttributes,

  supports: {
    anchor: false,
    className: true,
    customClassName: false,
  },

  description: __('A layout wrapper of content.'),

  getEditWrapperProps(attributes) {
    const { align } = attributes;

    if (-1 !== validAlignments.indexOf(align)) {
      return { 'data-align': align };
    }

    return {}
  },

  edit(props) {
    const { attributes, className, setAttributes, isSelected } = props
    // const { align } = props.attributes

    const onChangeBlockAlignment = (align) => {
      setAttributes({ align });
    }

    const onChangeClassName = (nextValue) => {
      setAttributes({ className: nextValue });
    }

    const onChangeAnchor = (nextValue) => {
      nextValue = nextValue.replace(ANCHOR_REGEX, '-');
      setAttributes({ anchor: nextValue });
    }

    return (
      <Fragment>
        {isSelected &&
          <BlockControls>
            <div style={blockNameStyle}>
              <small>{settings.title}</small>
            </div>
            <BlockAlignmentToolbar
              value={attributes.align}
              onChange={onChangeBlockAlignment}
              controls={validAlignments}
            />
          </BlockControls>
        }
        {isSelected &&
          <InspectorControls>
            <PanelBody
              title={__('Block Alignment')}
              className={'blocktype-container-block-alignment-inspector-controls'}
            >
              <BlockAlignmentToolbar
                value={attributes.align}
                onChange={onChangeBlockAlignment}
                controls={validAlignments}
              />
            </PanelBody>
            <BackgroundOptionsInspectorControls props={props} />
          </InspectorControls>
        }
        {isSelected &&
          <InspectorAdvancedControls>
            <TextControl
              label={__('Additional CSS Class', 'gutenberg')}
              value={attributes.className}
              onChange={onChangeClassName}
            />
            <TextControl
              label={__('HTML Anchor', 'gutenberg')}
              help={__('Anchors lets you link directly to a section on a page.', 'gutenberg')}
              value={attributes.anchor}
              onChange={onChangeAnchor}
            />
          </InspectorAdvancedControls>
        }
        <div
          className={classnames('entry-content', className, backgroundOptionsClasses(props))}
          style={{...{ position: 'relative', overflow: 'visible' }, ...backgroundOptionsInlineStyles(props)}}
        >
          {props.attributes.backgroundType === 'video' && props.attributes.backgroundVideo &&
            <div style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, overflow: 'hidden', }}>
              <BackgroundOptionsVideoOutput props={props} autoPlay={false} />
            </div>
          }
          <InnerBlocks layouts={{
            icon: 'editor-contract',
            label: 'Container',
            name: `container`,
          }} />
        </div>
      </Fragment>
    );
  },

  save(props) {
    // return null;
    // const { attributes } = props

    return <InnerBlocks.Content />;

    /*
    return (
      <div
        id={attributes.anchor}
        className={classnames(attributes.className, backgroundOptionsClasses(props), { [`align${attributes.align}`]: attributes.align })}
        style={backgroundOptionsInlineStyles(props)}
      >
        <BackgroundOptionsVideoOutput props={props} autoPlay={true} />
        <InnerBlocks.Content />
      </div>
    );
    */
  },
};

registerBlockType(name, settings)
