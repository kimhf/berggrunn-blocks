<?php
namespace Berggrunn\Blocks\Container;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Gutenberg block
 */
add_action('init', function () {
    $version = \Berggrunn\Blocks\Plugin::get_plugin_version();

    wp_register_script('berggrunn/scripts/blocks/container/editor', \Berggrunn\Blocks\Assets::asset_path('scripts/blocks/container/editor.js'), ['wp-blocks', 'wp-element'], $version, false);
    wp_register_style('berggrunn/styles/blocks/container/editor', \Berggrunn\Blocks\Assets::asset_path('styles/blocks/container/editor.css'), ['wp-edit-blocks'], $version);
    // wp_register_style('berggrunn/styles/blocks/container/main', \Berggrunn\Blocks\Assets::asset_path('styles/blocks/container/main.css'), null, $version);

    if (!function_exists('register_block_type')) {
        return;
    }

    register_block_type('berggrunn/container', array(
        'editor_script' => 'berggrunn/scripts/blocks/container/editor',
        'editor_style' => 'berggrunn/styles/blocks/container/editor',
        'style' => 'berggrunn/styles/components/main',
        'render_callback' => 'Berggrunn\\Blocks\\Container\\render',
        /* 'attributes' => [
            'className' => [
                'type' => 'string'
            ],
            'align' => [
                'type' => 'string'
            ],
            'anchor' => [
                'type' => 'string'
            ]
        ]*/
    ));
}, 20);

function render($attributes, $content = '')
{
    $args = wp_parse_args($attributes, [
        'className' => '',
        'align' => 'full',
        'anchor' => ''
    ]);

    $content = do_blocks($content);

    $container_classes = \Berggrunn\Components\BackgroundOptions\backgroundOptionsClasses($attributes);

    if ($args['className']) {
        $container_classes .= ' ' . $args['className'];
    }

    if ($args['align']) {
        $container_classes .= ' align' . $args['align'];
    }

    $background_inline_style = \Berggrunn\Components\BackgroundOptions\backgroundOptionsInlineStyles($attributes);

    $anchor = $args['anchor'] ? $args['anchor'] : '';

    return "<div id='{$anchor}' class='wp-block-berggrunn-container {$container_classes}' style='{$background_inline_style}'><div class='container-sub alignment-ready'>{$content}</div></div>";
}
