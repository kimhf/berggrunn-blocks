<?php
/**
 * Plugin Name: Berggrunn Blocks
 * Description: Add blocks using Gutenberg
 * Author: Kim Helge Frimanslund
 * Version: 0.0.0
 *
 * @package Berggrunn_Blocks
 */

namespace Berggrunn\Blocks;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Helper function for prettying up errors.
 * Based or error handling in Sage.
 *
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
function pretty_error($message, $subtitle = '', $title = '')
{
    $title = $title ?: __('Berggrunn Blocks &rsaquo; Error', 'sage');
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p>";
    wp_die($message, $title);
};

/**
 * Ensure dependencies and autoloader is loaded
 */
if (file_exists($composer = __DIR__.'/vendor/autoload.php')) {
    require_once $composer;
}

/**
 * BerggrunnBlocks required files
 *
 * The mapped array determines the code library included in the plugin.
 */
array_map(function ($file) {
    $file = __DIR__ . "/{$file}.php";
    if (!file_exists($file)) {
        pretty_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    } else {
        require_once($file);
    }
}, [
    'app/helpers',
    'app/actions',
    'resources/assets/blocks/collapsible/index',
    'resources/assets/blocks/profile/index',
    'resources/assets/blocks/container/index',
    'resources/assets/components/background-options/index',
    'resources/assets/shortcodes/_includes'
]);

/**
 * Get a array containing the config
 *
 * @return array The plugin config
 */
function setup_config()
{
    $plugin_dir  = __DIR__;
    $plugin_uri  = plugins_url('', __FILE__);
    $plugin_data = get_plugin_data(__FILE__, true, false);
    return [
        'plugin.uri'           => $plugin_uri,
        'plugin.dir'           => $plugin_dir,
        'plugin.version'       => $plugin_data['Version'],
        'language.text_domain' => $plugin_data['TextDomain'],
        'language.domain_path' => $plugin_data['DomainPath'],
        'assets.manifest'      => $plugin_dir . '/dist/assets.json',
        'assets.uri'           => $plugin_uri . '/dist',
        'assets.dir'           => $plugin_dir . '/dist',
    ];
}
