<?php
namespace Berggrunn\Blocks;

/**
 * Glimty-Gift-Reminders
 *
 *
 * @package Glimty-Gift-Reminders
 */

/**
 * @package Plugin
 */
class Plugin
{

    /**
     * Dir path to the plugin.
     *
     * @since   1.0
     *
     * @var   string
     */
    public $plugin_dir = null;

    /**
     * Url to the plugin.
     *
     * @since   1.0
     *
     * @var   string
     */
    public $plugin_uri = null;

    /**
     * The variable name is used as the text domain when internationalizing strings
     * of text. Its value should match the Text Domain file header in the main
     * plugin file.
     *
     * @since    0.0.1
     *
     * @var      string
     */
    protected $plugin_slug = 'berggrunn-blocks';

    /**
     * Instance of this class.
     *
     * @since    0.0.1
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Setup instance attributes
     *
     * @since     0.0.1
     */
    private function __construct()
    {
        // $this->plugin_version = BERGGRUNN_BLOCKS_PLUGIN_VERSION;
        $this->plugin_dir = plugin_dir_path(__DIR__);
        $this->plugin_uri = plugins_url($this->plugin_slug);
    }

    /**
     * Make properties accessible
     *
     * @param mixed $property
     * @return mixed
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Return the plugin uri.
     *
     * @since    0.0.1
     *
     * @return    Plugin uri variable.
     */
    public static function get_plugin_uri()
    {
        return self::get_instance()->__get('plugin_uri');
    }

    /**
     * Return the plugin slug.
     *
     * @since    0.0.1
     *
     * @return    Plugin slug variable.
     */
    public static function get_plugin_slug()
    {
        return self::get_instance()->__get('plugin_slug');
    }

    /**
     * Return the plugin version.
     *
     * @since    0.0.1
     *
     * @return    Plugin version variable.
     */
    public static function get_plugin_version()
    {
        return self::get_instance()->__get('plugin_version');
    }

    /**
     * Fired when the plugin is activated.
     *
     * @since    0.0.1
     */
    public static function activate()
    {
        // update_option( 'wpreactivate', 'Test Value' );
    }

    /**
     * Fired when the plugin is deactivated.
     *
     * @since    0.0.1
     */
    public static function deactivate()
    {
    }

    /**
     * Return an instance of this class.
     *
     * @since     0.0.1
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance()
    {

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}
