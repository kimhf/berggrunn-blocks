<?php
namespace Berggrunn\Blocks;

/**
 * Get paths for assets
 */
class Assets
{
    public static function asset_path($filename)
    {
        return self::get_asset_location($filename, config('assets.uri'));
    }

    public static function asset_dir($filename)
    {
        return self::get_asset_location($filename, config('assets.dir'));
    }

    private static function get_asset_location($filename, $prefix)
    {
        static $manifest;

        if (empty($manifest)) {
            $manifest = new JsonManifest(config('assets.manifest'));
        }

        if (array_key_exists($filename, $manifest->get())) {
            return $prefix . '/' . $manifest->get()[$filename];
        } else {
            return $prefix . '/' . $filename;
        }
    }
}
